import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

const request = "https://api.myjson.com/bins/1ario3";

Future<Map> getData() async {
  http.Response response = await http.get(request);
  var jsonData = json.decode(response.body);
  return jsonData;
}

class DadosPage extends StatefulWidget {
  @override
  _DadosPageState createState() => _DadosPageState();
}

class _DadosPageState extends State<DadosPage> {

  List multas;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //Primeira Página do APP
        body: FutureBuilder(
            future: getData(),
            builder: (context, snapshot){
              switch(snapshot.connectionState){
                case ConnectionState.none:
                case ConnectionState.waiting:
                  return Center(
                      child: Text("Carregando Dados...",
                        style: TextStyle(
                            color: Colors.blueAccent,
                            fontSize: 25.0),
                        textAlign: TextAlign.center,)
                  );
                default:
                  if (snapshot.hasError){
                    return Center(
                        child: Text("Erro ao Carregar Dados...",
                          style: TextStyle(
                              color: Colors.blueAccent,
                              fontSize: 25.0),
                          textAlign: TextAlign.center,)
                    );

                  } else{
                    multas = snapshot.data["Formulario2"];
                    var valores;
                    var soma = 0.0;
                    var somatotal = 0.0;
                    var somajuros = 0.0;
                    var somaigmp = 0.0;

                    for (var i =0; i<68; i++){
                      valores = multas[i]["Multas R\$"];
                      valores = double.parse(valores);
                      soma += valores;
                    }

                    for (var i =0; i<68; i++){
                      valores = multas[i]["Total A pagar"];
                      valores = double.parse(valores);
                      somatotal += valores;
                    }

                    for (var i =0; i<68; i++){
                      valores = multas[i]["Juros R\$"];
                      valores = double.parse(valores);
                      somajuros += valores;
                    }

                    for (var i =0; i<68; i++){
                      valores = multas[i]["IGMP"];
                      valores = double.parse(valores);
                      somaigmp += valores;
                    }

                    double porcentagem = num.parse(((soma/somatotal)*100).toStringAsPrecision(2));

                    return SingleChildScrollView(
                      padding: EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Image.asset("images/logo.png",
                            scale: 3.0, width: 120.0, height: 120.0,
                          ),
                          SizedBox(height: 16.0,),
                          Text(
                            "Total de Multas:\nR\$ $soma",
                            style: TextStyle(color: Colors.blueAccent,
                            fontSize: 25.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 16.0,),
                          Text(
                            "Total das Contas:\nR\$ $somatotal",
                            style: TextStyle(color: Colors.blueAccent,
                              fontSize: 25.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 16.0,),
                          Text(
                            "Porcentagem:\n$porcentagem \%",
                            style: TextStyle(color: Colors.blueAccent,
                              fontSize: 25.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 16.0,),
                          Text(
                            "Juros:\n$somajuros",
                            style: TextStyle(color: Colors.blueAccent,
                              fontSize: 25.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 16.0,),
                          Text(
                            "IGMP:\n$somaigmp",
                            style: TextStyle(color: Colors.blueAccent,
                              fontSize: 25.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    );
                  }
              }
            })
    );
  }}


