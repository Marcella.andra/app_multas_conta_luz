import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

const request = "https://api.myjson.com/bins/1ario3";

class InfoPage extends StatefulWidget {
  @override
  _InfoPageState createState() => _InfoPageState();
}

class _InfoPageState extends State<InfoPage> {

  List total;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: getData(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return body(snapshot.data);

          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }


  Widget body(Map data) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 16.0,),
          Text('Conta 1',
          style: TextStyle(fontSize: 20.0, color: Colors.blueAccent, fontWeight: FontWeight.bold),
          ),
          Container(
            // height: MediaQuery.of(context).size.height,
            child: Padding(
              padding: const EdgeInsets.all(14.0),
              child: Table(
                border: TableBorder.all(width: 1.0, color: Colors.black),
                children: [
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('N° da Instalação'),
                          new Text(data['Formulario2'][0]['NÂ° da InstalaÃ§Ã£o'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('Tensão Nominal (V)'),
                          new Text(data['Formulario2'][0]['TensÃ£o Nominal (V)'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('Consumo (KWh)'),
                          new Text(data['Formulario2'][0]['Consumo (KWh)'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('Total a pagar'),
                          new Text(data['Formulario2'][0]['Total A pagar'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('Multas'),
                          new Text(data['Formulario2'][0]['Multas R\$'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('Juros'),
                          new Text(data['Formulario2'][0]['Juros R\$'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('IGMP'),
                          new Text(data['Formulario2'][0]['IGMP'].toString()),
                        ],
                      ),
                    )
                  ])
                ],
              ),
            ),
          ),
          SizedBox(height: 25.0),
          Text('Conta 2',
            style: TextStyle(fontSize: 20.0, color: Colors.blueAccent, fontWeight: FontWeight.bold),
          ),
          Container(
            // height: MediaQuery.of(context).size.height,
            child: Padding(
              padding: const EdgeInsets.all(14.0),
              child: Table(
                border: TableBorder.all(width: 1.0, color: Colors.black),
                children: [
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('N° da Instalação'),
                          new Text(data['Formulario2'][1]['NÂ° da InstalaÃ§Ã£o'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('Tensão Nominal (V)'),
                          new Text(data['Formulario2'][1]['TensÃ£o Nominal (V)'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('Consumo (KWh)'),
                          new Text(data['Formulario2'][1]['Consumo (KWh)'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('Total a pagar'),
                          new Text(data['Formulario2'][1]['Total A pagar'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('Multas'),
                          new Text(data['Formulario2'][1]['Multas R\$'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('Juros'),
                          new Text(data['Formulario2'][1]['Juros R\$'].toString()),
                        ],
                      ),
                    )
                  ]),
                  TableRow(children: [
                    TableCell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text('IGMP'),
                          new Text(data['Formulario2'][1]['IGMP'].toString()),
                        ],
                      ),
                    )
                  ])
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
  Future<Map> getData() async {
    http.Response response = await http.get(request);
    var jsonData = json.decode(response.body);
    return jsonData;
  }

}
