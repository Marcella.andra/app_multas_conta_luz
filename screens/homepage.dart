import 'package:contasguamare_app/tabs/home_tab.dart';
import 'package:contasguamare_app/widgets/custom_drawer.dart';
import 'package:flutter/material.dart';
import 'dados_page.dart';
import 'grafico_page.dart';
import 'info_page.dart';

class HomePage extends StatelessWidget {

  final _pageController = PageController();
  @override

  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        //Primeira Página do APP 
        Scaffold(
          appBar: AppBar(
            title: Text("Prefeitura de Guamaré"),
            centerTitle: true,
            backgroundColor: Colors.blueAccent,
          ),
          drawer: CustomDrawer(_pageController),
          body: HomeTab(),
        ),
        //Segunda Pagina do APP 
        Scaffold(
          appBar: AppBar(
            title: Text("Contas de Energia Elétrica"),
            centerTitle: true,
            backgroundColor: Colors.blueAccent,
          ),
          drawer: CustomDrawer(_pageController),
          body: DadosPage(),
        ),
        //Terceira pagina do APP
        Scaffold(
          appBar: AppBar(
            title: Text("Gráficos"),
            centerTitle: true,
            backgroundColor: Colors.blueAccent,
          ),
          drawer: CustomDrawer(_pageController),
          body: GraphPage(),

        ),
        //Quarta Página do APP
        Scaffold(
          appBar: AppBar(
            title: Text("Informações Adicionais"),
            centerTitle: true,
            backgroundColor: Colors.blueAccent,
          ),
          drawer: CustomDrawer(_pageController),
          body: InfoPage(),
        ),
      ],
    );
  }

}




