import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pie_chart/pie_chart.dart';

const request = "https://api.myjson.com/bins/1ario3";

Future<Map> getData() async {
  http.Response response = await http.get(request);
  Map<String,dynamic> map = json.decode(response.body);
  return map;
}

class GraphPage extends StatefulWidget {
  @override
  _GraphPageState createState() => _GraphPageState();
}

class _GraphPageState extends State<GraphPage> {

  List total;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return Center(
                    child: Text("Carregando Dados...",
                      style: TextStyle(
                          color: Colors.blueAccent,
                          fontSize: 25.0),
                      textAlign: TextAlign.center,)
                );
              default:
                if (snapshot.hasError) {
                  return Center(
                      child: Text("Erro ao Carregar Dados...",
                        style: TextStyle(
                            color: Colors.blueAccent,
                            fontSize: 25.0),
                        textAlign: TextAlign.center,)
                  );
                } else {

                  total = snapshot.data["Formulario2"];
                  Map<String, double> dataMap = new Map();
                  Map<String, double> dataCon = new Map();

                  var valores;
                  var consumo;
                  for (var i =0; i<7; i++){
                    valores = total[i]["Total A pagar"];
                    valores = double.parse(valores);
                    dataMap.putIfAbsent('Total: R\$ $valores',() => valores);
                  }

                  for(var i =0; i<7; i++) {
                    consumo = total[i]["Consumo (KWh)"];
                    consumo = double.parse(consumo);
                    dataCon.putIfAbsent("Consumo: $consumo KWh", () => consumo);
                  }

                  List<Color> colorList = [
                    Colors.red,
                    Colors.green,
                    Colors.blueAccent,
                    Colors.yellow,
                    Colors.pink[200],
                    Colors.orange,
                    Colors.purple,
                  ];


                  return SingleChildScrollView(
            padding: EdgeInsets.all(16.0),
            child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
            Image.asset("images/logo.png",
            scale: 3.0, width: 120.0, height: 120.0,
            ),
            SizedBox(height: 16.0,),
              Text(
                "Valor total das contas: ",
                style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 16.0,
                ),
              ),
              SizedBox(height: 16.0,),
              PieChart(
                dataMap: dataMap, //Required parameter
                legendFontColor: Colors.blueAccent,
                legendFontSize: 14.0,
                legendFontWeight: FontWeight.w500,
                animationDuration: Duration(milliseconds: 800),
                chartLegendSpacing: 32.0,
                chartRadius: MediaQuery
                    .of(context)
                    .size
                    .width / 2.7,
                showChartValuesInPercentage: true,
                showChartValues: true,
                showChartValuesOutside: true,
                chartValuesColor: Colors.blueGrey[900].withOpacity(0.9),
                colorList: colorList,
                showLegends: true,
              ),
              SizedBox(height: 16.0,),
              Text(
                "Consumo (KWh): ",
                style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 16.0,
                ),
              ),
              SizedBox(height: 16.0,),
              PieChart(
                dataMap: dataCon, //Required parameter
                legendFontColor: Colors.blueAccent,
                legendFontSize: 14.0,
                legendFontWeight: FontWeight.w500,
                animationDuration: Duration(milliseconds: 800),
                chartLegendSpacing: 32.0,
                chartRadius: MediaQuery
                    .of(context)
                    .size
                    .width / 2.7,
                showChartValuesInPercentage: true,
                showChartValues: true,
                showChartValuesOutside: true,
                chartValuesColor: Colors.blueGrey[900].withOpacity(0.9),
                colorList: colorList,
                showLegends: true,
              ),



            ],

            ),
            );
          }
          }
          }
      ),
    );
  }
}
