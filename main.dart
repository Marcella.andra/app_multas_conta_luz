import 'package:contasguamare_app/screens/home_page.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'models/user_model.dart';


void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModel<UserModel>(
      model: UserModel(),
      child: MaterialApp(
        title: "Prefeitura de Guamaré",
        theme: ThemeData(
          primarySwatch: Colors.lightBlue,
          primaryColor: Colors.blueAccent,
          hintColor: Colors.black,
        ),
        debugShowCheckedModeBanner: false,
        home: HomePage(),
      ),
    );
  }
}
