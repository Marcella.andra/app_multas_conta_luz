import 'package:contasguamare_app/models/user_model.dart';
import 'package:contasguamare_app/screens/login_page.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Image.asset("images/logo.png"),
            SizedBox(height: 25.0),
            ScopedModelDescendant<UserModel>(
              builder: (context, child, model){
                return SizedBox(
                  height: 44.0,
                  child: RaisedButton(
                    child: Text(
                      !model.isLoggedIn() ?
                      "Entrar" : "Sair",
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                    textColor: Colors.white,
                    color: Colors.blueAccent,
                    onPressed: (){
                      if(!model.isLoggedIn())
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context)=>LoginPage())
                        );
                      else
                        model.signOut();
                    },
                  ),
                );
              },
            ),
            SizedBox(height: 16.0,),

              ],
        ),
      ),
    );
  }
}
